$(document).ready(function () {
    $(document).on("scroll", onScroll);

});

function onScroll(event){
    var scrollPos = $(document).scrollTop();

    //select each link
    $('#main-nav a').each(function () {
        var currLink = $(this);
        //get target URL
        var refElement = $(currLink.attr("href"));
        //determine if it is visible
        if (refElement.position().top <= scrollPos && refElement.position().top + refElement.height() > scrollPos) {
            //add active status for looked link
            currLink.addClass("active");
        }
        else{
            //otherwise, remove active status
            currLink.removeClass("active");
        }
    });
}
    $('.hash-header')
      .visibility({
        onTopPassed: function(calculations) {
            var m_header = $(this);
          $(".hash-link").each(function() {
              var link = $(this);
              if(link.prop("hash") == '#' + m_header.attr('id')){
                  link.removeClass("active")
              }
          })
        }
      })
    ;



.main.container img:not(.screenshot) {
      margin: 1em;
    }