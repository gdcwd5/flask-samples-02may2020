from flask import Flask, render_template, redirect, url_for, send_file, request
from jinja2 import Markup
import datetime
from pymongo import MongoClient
from os import environ
import json

app = Flask(__name__)

app.jinja_env.globals['include_raw'] = lambda filename : Markup.escape(app.jinja_loader.get_source(app.jinja_env, filename)[0])

class mongo:
    if environ.get('MONGO_KEY') is not None:
        username = "flaskwebsite"
        password = environ['MONGO_KEY']
        url = "mongodb+srv://flaskwebsite:"
        url = url + password
        url = url + "@gdc-d6ijq.gcp.mongodb.net/test?retryWrites=true&w=majority"
        client = MongoClient(url)
    else:
        client = MongoClient(port=27017)

    if environ.get('MONGO_ENV') is not None:
        db = client.website
    else:   
        db = client.stagewebsite

def log_request(p_req):
    visit = {
        'url' : p_req.url,
        'ip' : p_req.remote_addr,
        'time' : datetime.datetime.utcnow(),
        'agent' :  str(p_req.user_agent)
    }
    result = mongo.db.views.insert_one(visit)

    finaldict = dict()
    fullrequest = p_req.headers.environ.items()
    for (key, value) in fullrequest:
        prefix = str(key)[:4]
        if prefix not in {"wsgi", "werk", "guni"}:
            newkey = str(key).replace(".", "-")
            finaldict[newkey] = value
    finaldict['timestamp'] = datetime.datetime.utcnow()
    result2 = mongo.db.full.insert_one(finaldict)


@app.route('/')
def index():
    log_request(request)
    return render_template('index.html')

@app.route('/api/post')
def api_post_fax():
    log_request(request)
    return render_template('api/post.html')

@app.route('/api/get')
def api_get_fax():
    log_request(request)
    return render_template('api/get.html')

@app.route('/fax-server')
def fax_server():
    log_request(request)
    return render_template('fax-server.html')

@app.route('/api/cancel')
def api_cancel():
    log_request(request)
    return render_template('api/cancel.html')

@app.route('/api/delete')
def api_delete():
    log_request(request)
    return render_template('api/delete.html')

@app.route('/api/callback')
def api_callback():
    log_request(request)
    return render_template('api/callback.html')

@app.route('/api/r/fax')
def api_fax_r():
    log_request(request)
    return render_template('api/fax-r.html')

@app.route('/api/r/fax-media')
def api_fax_media_r():
    log_request(request)
    return render_template('api/fax-media-r.html')

@app.route('/api/r/status')
def api_status():
    log_request(request)
    return render_template('api/status-call.html')

@app.route('/api/')
def api_index():
    log_request(request)
    return render_template('api/index.html')

@app.route('/resume')
def gdc_resume():
    log_request(request)
    return render_template('resume.html')

@app.route('/blog/tf')
def blog_tf():
    log_request(request)
    return send_file('static/tf-diagram-06MAY2020.pdf', attachment_filename='tf-diagram-06MAY2020.pdf')

@app.route('/code/')
def code_index():
    log_request(request)
    return render_template('code/index.html')

@app.route('/code/django/')
def code_django():
    log_request(request)
    return render_template('code/django.html')

@app.route('/code/cpp/')
def code_cpp():
    log_request(request)
    return render_template('code/cpp.html')

@app.route('/blog/')
def blog_index():
    log_request(request)
    return render_template('blog/remote-short.html')

@app.route('/blog/remote-old/')
def blog_remote_old():
    log_request(request)
    return render_template('blog/remote.html')

@app.route('/blog/remote/')
def blog_remote():
    log_request(request)
    return render_template('blog/remote-short.html')

if __name__ == '__main__':
    app.run()
