﻿# Setup a Fax Server
ICTFax is open source software for sending and receiving faxes. ICTFax handles multiple incoming phone numbers, multiple users, and routing incoming. Users can send faxes using the web interface, or with special email addresses. 
For example, an email from a registered email address to 18002752273@fax.mydomain.com may be received by the fax server and transmitted to the phone number. 
- terms: VM, hostname, ICTFax, ICTFax Modules
# Configure Server
## Setup a Virtual Machine
Setup ICTFax in a dedicated virtual machine (VM). Roughly 2gb of memory and 2 CPU cores is sufficient for a small number of active users. ICTFax is configured to run on CentOS 7, and has not been tested with other platforms.
The VM should be accessible over the internet on ports 22 (SSH), 25 (SMTP- send mail), and 80 (http webserver). 
The VM needs a hostname. Appropriate hostnames include “fax.mydomain.com” or “faxdomain.com” are recommended. 
Configure MX records for the hostname. Mail for the hostname (e.g., @fax.mydomain.com) should be directed to the IP address of the VM. 
- breakout -- digital ocean vm 
- breakout -- domain
- breakout -- mx

Open a SSH connection to the VM. Connect as root, or appropriately use ‘sudo’ in the shell commands below.
## Install Fax Server Core
Disable SELinux (Secure Linux) on the VM. 

- code -- check status, then disable.

ICTFax requires multiple dependencies. 
Web hosting dependencies include: Apache 2,  MySQL 5 or MariaDB, and PHP 5. 

- code

ICTFax also requires FreeSWITCH, and Sendmail. 
Add freeswitch and ICTFax repositories:

- code

Install FreeSwitch.

- code

Install MariaDB locally as the database for ICTFax. Installation scripts directed to email delivery depend on a local database instance. 

- expand?
- code

Install the core modules for ICTFax:
- code
## Setup Database
Connect to the database, then setup the ICTFax table and user. 
If using the default local MariaDB instance, connect with “mysql”
Create ICTFax Database and User. Make note of the database name, database username, and associated password. 
- code

Import ICTFax Schema and Demo Data.
- code

Configure ICTFax with the database name, database username, and associated password. 
Open ictcore.conf
- code -- sudo nano /usr/ictcore/etc/ictcore.conf 

Under the [db] section, update the database name, username, and password. 
- code diff view?

## Setup SMTP to Receive Emails
Configure sendmail.mc to receive mail on all interfaces (e.g.., IP addresses). Receiving mail using SMTP requires port 25 open on the VM. 
- code -- nano
- code -- diff of config

Add ICTFax to the trusted users for receiving mail.
- code

Add your domain to the allowed local domain list, used for handling incoming emails. 
- code

Configure sendmail to route all emails to ICTFax. Emails addressed based on phone numbers (e.g., @fax.org) will be routed to ICTFax.  
- code

Update sendmail, and restart the service. This may take 1-3 minutes.
- code

Update the configuration of ICTFax, connecting it to the incoming email. 
- code

Import the email to fax handling rules. For example, @myfax.org will be routed to the phone number 1-800-275-2273. This requires using the default MariaDB instance. 
- code file script for DB
## Setup Web Interface
Install the ICTFax web interface. 
- code

Restart the apache web server. 
- code

Accessing the web interface requires port 22 open on the VM.
# Administrative Tasks
## Access Web Interface
Visit the URL of the web interface, using the hostname configured for the VM.
- code

The database includes a default login.  
- code

## Setup Fax Provider
ICTFax requires a SIP fax provider. ICTFax will connect to the provider to transmit the fax. Also, ICTFax will register with provider using SIP. The provider will initiate connections back to ICTFax for receiving faxes. 
ICTFax establishes one ‘trunk’ connection to the provider. This connection is sufficient for one fax connection at at time. However, the provider can route faxes for multiple numbers over the single trunk connection. 
Configure phone numbers at the SIP provider, which may be referred to as “DID” (direct inbound dial) numbers. Phone numbers must also be added at ICTFax. 
T38fax is a compatible SIP provider. 
- sip provider, sip settings
- breakout -- sip
- breakout -- sip trunk, sip DID
- https://www.t38fax.com/pricing/
## Configure Users and Phone Numbers
- ez
## Setup Incoming Faxes by Email
- will be sent to users automatically? Note -- spam
## Setup Transmit Faxes by Email
- user registered email address, as shown above