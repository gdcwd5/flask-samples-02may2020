[[SELinux Status]]
getenforce
setenforce 0

[[add repos]] [[preqs -- hard]]
yum install -y https://service.ictinnovations.com/repo/7/ict-release-7-4.el7.centos.noarch.rpm
yum install -y http://files.freeswitch.org/freeswitch-release-1-6.noarch.rpm

yum install apache php
yum install mariadb
yum install -y epel-releas
yum install freeswitch

[[reqs]]
CentOs 7
Apache 2
MySQL 5
PHP 5.3.3
ICTCore
Sendmail
FreeSWITCH
- may need to install nano

[[add core modules]]
yum -y install ictcore ictcore-fax ictcore-email
# worked after retry?

[[connect to db?]]
mysql

[[create user]]
CREATE DATABASE ictfax;
USE ictfax;
GRANT ALL PRIVILEGES ON ictfax.* TO ictfaxuser@localhost IDENTIFIED BY 'plsChangeIt';
FLUSH PRIVILEGES;

[[import schema]]
SOURCE /usr/ictcore/db/database.sql;
SOURCE /usr/ictcore/db/fax.sql;
SOURCE /usr/ictcore/db/email.sql;
SOURCE /usr/ictcore/db/data/role_user.sql;
SOURCE /usr/ictcore/db/data/role_admin.sql;
SOURCE /usr/ictcore/db/data/demo_users.sql;

quit

[[configure db connection -- nano]]
nano /usr/ictcore/etc/ictcore.conf

[[code]]
user = ictfaxuser
pass = plsChangeIt
name = ictfax
# note - deltaview? redline?

[[mx records]]
??? setup
hostname "fax"
corresponds to fax.domain.org

mail providers server
fax.clinemail.info
point to your server

priority
10, default

TTL
keep at default
time in seconds of expire record

# move to start?
# also note A record?

[[configure sendmail -- nano]]
nano /etc/mail/sendmail.mc

[[code]]
# delta view
# remove
DAEMON_OPTIONS(`Port=smtp,Addr=127.0.0.1, Name=MTA')dnl
# add
DAEMON_OPTIONS(`Port=smtp, Addr=0.0.0.0, Name=MTA')dnl

- was already done?


[[add to trusted users]]
echo "ictcore" >> /etc/mail/trusted-users
echo "apache" >> /etc/mail/trusted-users


[[add domain for mail]]
# must configure ! !
echo "FAX_DOMAIN.COM" >> /etc/mail/local-host-names


[[configure sendmail handling to ICT]]
echo '@FAX_DOMAIN.COM ictcore' >> /etc/mail/virtusertable

[[update and restart sendmail]]
/etc/mail/make
chkconfig sendmail on
# request was fwd?
service sendmail restart
# explain these steps more?
# also fwd
# took a while? at least one min

[[edit mailbox location config -- nano]]
nano /usr/ictcore/etc/ictcore.conf

[[code]]
# delta view
# under heading [sendmail]
# was already?
folder = /var/spool/email/ictcore

[[import fax handling rules]]
cd /usr/ictcore/bin/sendmail
./email_to_fax

[[install fax web interface]]
yum install ictfax

[[restart apache]]
service httpd restart

[[admin url?]]
http://yourdomain/ictfax

[[default login]]
admin@ictcore.org
helloAdmin

[[sip settings]]
???

[[transmit fax by email]]
# structure of email?

# screenshots
- fax provider
- configure user
-- email is important
- configure phone number



