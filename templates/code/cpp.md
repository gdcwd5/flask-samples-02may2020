# introduction
C++ is commonly used for numerical modeling (i.e., solving complex math problems). Matrix mathmatics is cumbersome, but powerful. 

This informal library provides matrix data structures, and defines related mathmatical operations. For example, matrix multipliation and matrix scaling.   

This library utelizes C++ incluing an abstract base class, and templating. 

The "aMatrix" abstract base class allows operations to be performed on matrixes of varying types. 

Common matrix types include dense, triangular, and diagional. A diagional matrix has values only along the diagional, and zeroes everywhere else. A triangular matrix includes values along the diagional, and addiotnally non-zero values either above or below the diagional. A dense matrix can have values anywhere. 

For example, a diagional matrix could be stored as a dense matrix. However, this is memory inefficent. A dense 4x4 matrix is allocated space for 16 values. A diagional 4x4 matrix is allocated only space for 4 values. 

It's efficent to use diagional matrixes where possible. However, it's necessary to perform operations on both dense and more specific matrix types. For example, the matrix multiplcation function is defined to accept two abstract matrixes. That is, any matrix based on the aMatrix abstract base class. Thus, you can multiply a dense matrix by a diagional matrix. 

Additionally, all of the matrix classes are templated. In other words, when creating a matrix object, you provide a type identifier (e.g., integer, float, double). This type identifier is used to create the objects within the matrix. For example, you can have a matrix of integers or float values. 

Notably, the template paramater is seperate from the matrix type. For example, you can have a diagional matrix of integers, or a triangular matrix of floats. 

## abstract base class


### member function
The abstract base class (aMatrix) lists member functions that all child classes have to implement. For example, all child classes of aMatrix have to implement matrix.get(i,j), where i and j are indexes of a location in the matrix. 

Thus, we can define matrix multiplication between two aMatrixes, knowing that regardless of the actual type of matrix eventually used, they will implement get(i,j). 

Additionally, we will define a function element(i,j) that returns a reference, allowing you to modify values in the matrix. 

The size() and valid(i,j) functions make accessing the matrix easier. 

### friend functions
Related to the abstract base class, we are defining functions that accept take aMatrix as input paramaters. In other words, they accept any matrix type based on aMatrix as an input.

For example, the operator*(l, r) function allows you to multiply two matrixes of any type. A dense matrix is returned. 

Because we don't know the type of the matrixes input, we can only use functions declared at the aMatrix level. For example, since aMatrix requires size(), get(), and element(), we can use those functions. The child classes of aMatrix, like diagMatrix, are free to implement theese functions in their own way. 

# implementation classes

## diag matrix
Let's take a look at an example implementation of class that inherits from aMatrix.

The diagMatrix class describes a diagional matrix, and is generally a sub-type of matrixes. 

The diagMatrix only stores the values along the diagional, but because it is based on aMatrix, you can use it alongside other types of matrixes. For example, you can multiply a dense matrix by a diagMatrix. 

### constructor
The aMatrix base class is abstract, so it doesn't have any member variables.

The diagMatrix class inehrits from aMatrix, but isn't abstract, it actually implements a matrix. So, it has member variables, and a constructor. 

The constructor, diagMatrix<T>::diagMatrix(i), allocates just enough memory to store the diagional, equal to the size of the matrix. Dense matrixes have data allocated for each location in the matrix, equal to the size squared.

### helper functions
Before we get to the access/modify functions (e.g., element and get), some helper functions are defined. They make the accessor functions easier to read. 

Valid indicates if a provided index is within the matrix. For example, (-1, 38) is not a vald matrix index. 

Defined indicates if the index points to a location that has memory allocated. For example, in a diagional matrix, every location besides the diagional is zero, and thus doesn't have memory allocated. 

The map function transforms a matrix index, such as (i,j), to an array index. In this implementation, the underlying data structure is an array. More specifically, the values of the diagional are stored as an array. The map function takes a matrix index and maps it to a location in the array. 

### access functions
Now, consider the access function get(i, j). It's quite readable with some helper functions defined.

First, if the index is invalid, we throw an error.

If the index is valid, and also defined, we use the map function to retreive the value from our array data structure. 

If the index is valid, but not defined, we simply return zero. For example, any index not along the diagional is zero, in this type of matrix.

Note this get() function returns a copy of the value from the matrix. 

- element 

If we want to retreive a modifiable reference, the logic is slightly different.

We can't return a modifiable reference to a location that must be zero. Thus, we throw an error if the location is invalid, or not defined. 

# special cases
- interesting property of templating
- multiply

The general matrix multiplication function, for two aMatrixes, is described above.

However, a useful feature of C++ is that you can 'overload' functions. In other words, you can create a sepearte implementation for a particular situation. 

While our general aMatrix multiplications works generally, we can define a seperate function to more efficiently handle a specific situation.

Generally, when you multiply a diagional matrix by a triangular matrix, you get another triangular matrix as a result. With this knowledge, we can define another matrix multiplication function, that will return a more efficent triangular matrix as the result.

C++ will automatically use this more efficent function where possible, because the syntax is the same.

result = densematrix * mytriangle //uses the definition from the aMatrix level
result = mydiag * mytriangle  //uses the 'overloaded' definition we additionally defined

In the below example, we use result.defined() to skip calculating values that will be zero. 

# testing
A matrix library is only useful if generates accurate results. 

Creating unit tests is an efficent way to check that your library is working properly. 

This example uses Boost Unit Tests, and test matrix multiplicaiton for dense matrixes. 

First, setup test data. d1 and d2 are the test data used to create the two matricies that will be multiplied together. d3 and d4 store the correct results, used to check the result of our multiplication. 

First, create the two matrixes tc1 and tc2 from the test data. 

Then, create the multiplication results, using the * operator defined above. 

Finally, compare the result of the multiplication to the known correct data stored in d3 and d4. 

The BOOST_CHECK_EQUAL(a, b) function checks if two values are equal.

When you run the test suite, boost handles outputting the results. For example, it could indicate all test cases passed, or the 'dm_mult' test case failed. It's a good practice to run the tests before committing, to make sure you haven't accidentially broken some functionality. 