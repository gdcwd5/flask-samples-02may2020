template<typename T>
class aMatrix{
public:

    virtual ~aMatrix() {}

    virtual inline bool valid(const int i, const int j) const = 0;

    virtual T& element(const int i, const int j) = 0;

    virtual T get(const int i, const int j) const = 0;
 
    virtual int size() const = 0;

};

#include "aMatrix.hpp"