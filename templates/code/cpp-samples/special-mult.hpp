template<typename T>
tMatrix<T> operator*(const diagMatrix<T>& l, const tMatrix<T>& r){

    int size = l.size();

    if(l.size() != r.size())
        throw std::logic_error("invalid size for matrix multiply");

    tMatrix<T> result(l.size());

    int ops = 0;

    for(int i = 0; i < size; i++)
        for(int j = 0; j < size; j++)
        {
            if(result.defined(i,j))
                result.element(i,j) = l.get(i,k) * r.get(k,j);
            }
        }

    return result;
}