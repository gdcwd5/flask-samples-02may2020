template<typename T>
dMatrix<T> operator*(const aMatrix<T>& l, const aMatrix<T>& r){
    if(l.size() != r.size())
        throw std::logic_error("invalid size for matrix multiply");

    int size = l.size();

    dMatrix<T> result(size);

    int ops = 0;

    for(int i = 0; i < size; i++)
        for(int j = 0; j < size; j++)
        {
            ops++;
            result.element(i,j) = l.get(i,j) * r.get(i,j);
        }

    cout << "# [" << l.size() << "," << r.size() <<"] operations: " << ops << endl;
    return result;

}