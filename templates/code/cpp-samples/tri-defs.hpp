class tMatrix : public aMatrix<T>
{
    //tMatrix inherits from aMatrix

    //function declarations
};

//helper functions
template<typename T>
inline int tMatrix<T>::map(const int i, const int j) const{
    if(!valid(i,j))
        throw std::logic_error("thingy");

    return j - max(0,i - 1) + max(0,2 + (i-1)*3);
}

template<typename T>
inline bool tMatrix<T>::valid(const int i, const int j) const{
    return i >= 0 && j >= 0 && j < m_size && i < m_size;
}

template<typename T>
inline bool tMatrix<T>::defined(const int i, const int j) const{
    return i <= j + 1 && j <= i + 1 && valid(i,j);
}

// key function definition required by the abstract base class

template<typename T>
T tMatrix<T>::get(const int i, const int j) const {
    if(!valid(i,j))
        throw std::logic_error("invalid access");

    if(defined(i,j))
        return m_data[map(i,j)];
    else
        return 0;
}

template<typename T>
T& tMatrix<T>::element(const int i, const int j) {
    if(!valid(i,j) || !defined(i,j))
        throw std::logic_error("invalid access");

    return m_data[map(i,j)];
}