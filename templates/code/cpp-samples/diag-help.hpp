
//helper functions
template<typename T>
inline bool diagMatrix<T>::valid(const int i, const int j) const{
    return i >= 0 && j >= 0 && j < m_size && i < m_size;
}

template<typename T>
inline bool diagMatrix<T>::defined(const int i, const int j) const {
    return i >= 0 && i == j;
}

template<typename T>
inline int diagMatrix<T>::map(const int i, const int j) const {
        if(!valid(i,j))
            throw std::logic_error("invalid access 5");
        return i;
}