#include <boost/test/unit_test.hpp>
#include "dMatrix.h"

using namespace std;

BOOST_AUTO_TEST_SUITE( dense_mult )

BOOST_AUTO_TEST_CASE( dm_mult )
{

    const int d1 [] = {1, 2, 3, 4};
    const int d2 [] = {2, 0, 1, 2};
    const int d3 [] = {4, 4, 10, 8};
    const int d4 [] = {2, 4, 7, 10};
    const int size = 2;

    dMatrix<int> tc1(size);
    dMatrix<int> tc2(size);

    int step = 0;
    for(int i = 0; i < size; i++)
        for(int j = 0; j < size; j++)
        {
            tc1.element(i,j) = d1[step]
            tc2.element(i,j) = d2[step]
        }
    }
    

    dMatrix<int> tc3(size);
    dMatrix<int> tc4(size);

    tc3 = tc1 * tc2;
    tc4 = tc2 * tc1;


    step = 0;
    for(int i = 0; i < tc3.size(); i++)
        for(int j = 0; j < tc3.size(); j++)
        {
            BOOST_CHECK_EQUAL(tc3.get(i,j), d3[step]);
            BOOST_CHECK_EQUAL(tc4.get(i,j), d4[step]);
            step++;
        }
    }

}

BOOST_AUTO_TEST_SUITE_END()