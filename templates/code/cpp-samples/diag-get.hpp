// key function definition required by the abstract base class
template<typename T>
T diagMatrix<T>::get(const int i, const int j) const {
    if(!valid(i,j))
        throw std::logic_error("invalid access 1");


    if(defined(i,j))
        return m_data[map(i,j)];
    else
        return 0;
    

}

template<typename T>
T& diagMatrix<T>::element(const int i, const int j){
    if(!valid(i,j) || !defined(i,j))
        throw std::logic_error("invalid access 3");

    return m_data[map(i,j)];
}