{% extends "code/base.html" %}
{% block content %}
<h1 id="intro">Introduction</h1>
<div class="ui positive message">
    <div class="header">
      Live App Available
    </div>
    <p>Go to <a href="http://fax.cline.group">fax.cline.group</a> to check out the Django app.</p>
  </div>
<p>Let's look at using Twilio to bring faxes into the web era.</p>
<p>Twilio is an innovative provider of communication APIs. Obtuse business actions (e.g., sending a fax) can be
    automated and integrated into Django. </p>
<p>Django is an excellent framework for rapidly developing business applications. Django automatically handles common
    and complex tasks, like user management, database models, and authentication. </p>
<p>This tutorial is targeted to someone who has completed the excellent <a href="https://www.djangoproject.com/start/">
        Django tutorial</a>. Overall, you should be familiar with how Django handles database models and structures files.</p>
<p>In this example, we are adding fax capability to a Django app. More specifically, we will be adding a fax model to
    the database. Then, you can interact with fax objects in your Django app. You can create views in Django for sending faxes and viewing recent ones. Twilio handles the complexity of actually
    sending the fax.</p>
<h2 id="twilio">Twilio Config</h2>
<p>As a preliminary matter, let's get the Twilio client (Twilio SDK) in python setup. The Twilio client is available
    from pip, (&quot;python -m pip install twilio&quot;). </p>
<div class="ui top attached code orange segment">
    <p>python, models.py file</p>
</div>
<div class="ui bottom attached code segment">
    {{ code_block('code/python-samples/twilio.py') }}
</div>

<p>I've chosen to create a class to hold the Twilio client, and the associated configuration variables. You can find the
    AccountSid (i.e., a unique identifier for your account) and AuthToken in the <a href="https://www.twilio.com/docs/iam/keys/api-key-resource">Twilio console</a>. </p>
<p>If you have a Twilio trial account, you can <a href="https://www.Twilio.com/docs/phone-numbers">get your first phone number</a> (used to send the faxes) for free. Double
    check that the phone number is enabled for faxes in the Twilio console. Use this as your sending phone number.
    </p>
<p>In this example, I'm working with phone numbers local to the United States <a href="https://en.wikipedia.org/wiki/North_American_Numbering_Plan">(e.g., North America Numbering Plan,
    NANP, numbers)</a>. These phone numbers have a length of 10, such as 8002752273. If you plan to work with phone numbers
    in another country, you will need to change the PhoneLen value, and review Twilio's guidelines on formatting
    phone numbers. </p>
<p>BaseUrl is the domain name and protocol Twilio will use to interact with your Django app. It needs to be accessible
    over the internet, so it's best to use the production protocol and hostname of your app. <a href="https://devcenter.heroku.com/articles/deploying-python">Heroku</a> and <a href="https://www.digitalocean.com/community/tutorials/how-to-set-up-a-scalable-django-app-with-digitalocean-managed-databases-and-spaces">DigitalOcean</a>
    provide affordable hosting options for your Django/Python project. </p>
<p>TwilioConfig.client is the object that we will use to communicate with Twilio. </p>
<p>With the connection to Twilio setup, we are ready to look at the Fax object. </p>
<h1 id="models">Django Model</h1>
<h2 id="fax-field">Example Fax Model</h2>
<div class="ui top attached code orange segment">
    <p>python, models.py file</p>
</div>
<div class="ui bottom attached code segment">
    {{ code_block('code/python-samples/models.py') }}
</div>
<p>We are ready to start defining the fax model (e.g., a table in the django database). First, I have defined some
    project specific fields. In the example implementation, the fax model has a field relating the fax object to a parent
    project (i.e., another Django model). The fax model also had a DateCreated, which defaults to the date the fax object was
    created. </p>
<p>To send a fax, we need both a destination phone number and a PDF of the document to send. The DestPhone field uses
    PhoneLen from the TwilioConfig (defined above). </p>
<p>Django handles storing files, but you should define a folder to organize the PDFs for faxes. You can change where
    exactly Django stores files using the <a href="https://docs.djangoproject.com/en/3.0/ref/settings/#static-files">MEDIA_ROOT property in settings.</a></p>
<p>Next, we need to define some fields specifically for communicating with the Twilio Fax API. </p>
<p>FaxStatus holds a string indicating the current status of the fax. The default value is 'ready', but we will
    update it as we send the fax and interact with Twilio later. </p>
<p>The TwilioID (also called SID, or FaxID) is string uniquely identifying a fax processed by Twilio. The TwilioID is
    used to get the status of the fax, and for other API interactions. </p>
<p>The TwilioError message is a long string used to hold an error message returned by Twilio. We will use this value
    along with the status when defining methods for the fax model. </p>
<h2 id="model-methods">Fax Model Methods</h2>
<h3>Send Fax Method</h3>
<div class="ui top attached code orange segment">
    <p>python, models.py file</p>
</div>
<div class="ui bottom attached code segment">
    {{ code_block('code/python-samples/model-send.py') }}
</div>
<p>Let's turn to actually sending the fax. More specifically, the Send() member function on fax object.</p>
<p>TwilioConfig.client is the Twilio Fax API client object, as defined above. We are going to create a new fax with the
    create() function provided by Twilio. This function requires 3 parameters: the sending number, the
    receiving number, and a full URL to a PDF to send. </p>
<p>The sending number must be enabled on your Twilio account, so check the Twilio console. In this example, the sending
    phone number is static and saved to the TwilioConfig object. </p>
<p>The destination number is retrieved from the Django fax object. For example, the fax object and the included phone
    number could be created using a HTML form or the Django Admin Console. </p>
<p>The URL of the PDF is the combination of the base hostname for your site, and the Django provided url for the PDF
    file. Twilio will download the PDF from this URL. In other words, Twilio will 'callback' to this URL. </p>
<p>If you have included proper parameters, TwilioConfig.client.create() returns a twilio.fax object. We are going to
    save the sid (i.e., the Twilio unique ID) back to our Django model. </p>
<p>If the parameters are invalid (e.g., invalid destination phone number), the Twilio client throws an exception, as identified above. </p>
<p>We are going to catch this exception, and update the status of the Django fax object to error. Also, the error
    message is saved in the TwilioErrorMessage field. Thus, you can identify faxes not sent in other Django views. </p>
<p>Remember to save the Django fax object after you are finished modifying it. </p>
<h3 id="update-status-method">Update Status Method</h2>
    <p>Below, we will define a view that updates the status of a fax object. This view will be used by Twilio to update the
        status as the fax is processed (e.g., sending, delivered, no answer).</p>
<div class="ui top attached code orange segment">
    <p>python, models.py file</p>
</div>
<div class="ui bottom attached code segment">
    {{ code_block('code/python-samples/model-update.py') }}
</div>

<p>Twilio can request to update the status rapidly. For example, a fax may rapidly change from &quot;queued&quot; to
    &quot;sending&quot;, and further to &quot;no answer&quot;. </p>
<p>To handle this, we need to lock the fax object while we are updating the status. Then, other attempts to update the
    status will wait for the current transaction to finish.</p>
<p>Note, this is supported with database backends such as MySql, and is not supported on the default Sqllite (i.e.,file)
    database. </p>
<p>Create a database transaction context using "with transaction.atomic()". Within the transaction, we query the
    database for the current fax object. While it seems redundant to query for the current object, it's
    necessary to properly lock the database record for the fax object. </p>
<p>Use the selected fax and update its status value. Outside the &quot;with&quot; block, the
    fax object is unlocked.</p>
<p>If a second request to update the fax value comes in while the first is being processed, it will wait and
    automatically begin after the first is finished. </p>
<h1 id="views">Django Views</h1>
<h2 id="status">Status Callback</h2>
<p>In the Twilio console, you can define a URL for status callbacks. When the status of an outgoing fax is updated,
    Twilio will callback to this URL. </p>
    <p>We need to create a Django view to handle this incoming request from Twilio.</p>
    <p>First, define this URL in the Django urls.py file. Connect the URL to a new view, and <a href="https://www.twilio.com/docs/video/api/status-callbacks">provide the URL to Twilio in the console.</a></p>
<div class="ui top attached code orange segment">
    <p>python, urls.py file</p>
</div>
<div class="ui bottom attached code segment">
    {{ code_block('code/python-samples/urls.py') }}
</div>
<p>The purpose of this view is to find the Django fax object Twilio is referencing, and copy the new status provided
    by Twilio back to the local fax object. Then, we provide an empty json response, indicating success. </p>
<div class="ui top attached code orange segment">
    <p>python, views.py file</p>
</div>
<div class="ui bottom attached code segment">
    {{ code_block('code/python-samples/views.py') }}
</div>
<p>First, we extract the FaxId from the request, and find the Django fax object with that has a matching TwilioId.</p>
<p>Then, we call the UpdateStatus() method on the found fax object, passing in the new status value provided by Twilio in
    the request. </p>
<p>Now, if you have a different view that lists faxes including their status, these status values will reflect the
    updated values provided by Twilio. </p>
<h1>Conclusion</h1>
<p>Start building more views and methods that interact with fax objects. For example, check out <a href="https://docs.djangoproject.com/en/3.0/topics/forms/"> Django's documentation on forms.</a></p>



{% endblock %}