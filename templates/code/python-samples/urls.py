from django.urls import path
from django.conf import settings
from django.conf.urls.static import static
from . import views

urlpatterns = [
    # . . . the other urls for your project . . .

    # path for twilio callback
    path('/fax-status/', views.api_fax_callback, name='api_fax_callback')
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
# also tell Django to serve the static files, like the Pdfs