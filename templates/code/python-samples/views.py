from django.http import JsonResponse, HttpResponse, HttpResponseRedirect
from django.core import serializers
from .models import Fax, Matter, Client
from django.views.decorators.csrf import csrf_exempt

@csrf_exempt # django won't require an auth token
def api_fax_callback(request):
    # incoming callbacks are POSTs
    if request.method == 'POST':
        fid = request.POST.get('FaxSid')
        fax = Fax.objects.get(TwilioId=fid)
        fax.UpdateStatus(request.POST.get('FaxStatus'))
    return JsonResponse({})

