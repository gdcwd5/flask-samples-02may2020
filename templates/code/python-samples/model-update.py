class Fax(models.Model):
    # model fields

    def UpdateStatus(self, p_status):
        # start a database transaction
        with transaction.atomic():
            # select and lock the fax
            query = Fax.objects.select_for_update().get(pk=self.pk)
            # update the status
            query.FaxStatus = p_status
            query.save()
        return self