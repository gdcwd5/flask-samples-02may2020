from django.db import models, transaction
import twilio.rest.Client 
import twilio.base.exceptions

class TwilioConfig:
    # auth variables
    AccountSid = 'ACd09bb53515a2e58d20c27b53a9424d13'
    AuthToken = 'e6b234745b5d7cb90df72bea593e6e58'

    # project configuration
    PhoneLen = 10
    BaseUrl = 'http://fax.cline.group'
    SendingPhoneNumber = '+12565872658'

    # the API interface/client
    client = twilio.rest.Client(AccountSid, AuthToken)