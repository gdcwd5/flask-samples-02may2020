class Fax(models.Model):
    # project specific fields
    ParentProject = models.ForeignKey(Project, on_delete=models.CASCADE)
    DateCreated = models.DateField(auto_now_add=True)

    # essential fields for faxes
    DestNumber = models.CharField(max_length=TwilioConfig.PhoneLen)
    Pdf = models.FileField(upload_to='pdfs/', null=True)

    # fields for data from twilio
    FaxStatus = models.CharField(max_length=10, default='ready')
    TwilioId = models.CharField(max_length=50, null=True)
    TwilioErrorMessage = models.TextField(null=True)
