class Fax(models.Model):
    # model fields

    def Send(self):
        try:
            sent_fax = TwilioConfig.client \
                .create(
                    from_= TwilioConfig.SendingPhoneNumber,
                    to='+1' + str(self.DestNumber),
                    media_url= TwilioConfig.BaseUrl + str(self.Pdf.url)
                )
            self.TwilioId = sent_fax.sid
        # catch exception if fax can't be created
        except twilio.base.exceptions.TwilioRestException as e:
            self.FaxStatus = "error"
            self.TwilioId = ""
            # save the twilio error message
            self.TwilioErrorMessage = e.msg
        self.save()

        return self