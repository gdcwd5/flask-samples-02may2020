# tiny intro / title?

# introduction

Django is an excellent framework for rapidly developing business applications. Django automatically handles common and complex tasks, like user management, database models, and authentication. 

This tutorial is targeted to someone who has completed the Django 'building your first app' tutorial (which I highly reccommend). In other words, a familarility with how django defines database models and structures files is expected. 

Twilio is an innovative provider of communication APIs. Obtuse business actions (e.g., sending a fax) can be automated and integrated into Django. 

In this examlpe, we are adding fax capability to a django app. More specifically, we will be adding a fax model to the database. Then, you can interact with fax objects in your django app. Twilio handles the complexity of actually sending the fax.

## twilio config

As a preliminary matter, let's get the Twilio client (Twilio SDK) in python setup. The Twilio client is availabe from pip, ("python -m pip install twilio"). 

I've chosen to create a class to hold the twilio client, and associated configuration variables. You can find the AccountSid (unique identifier for your account) and AuthToken in the Twilio console(link). 

If you have a twilio trial account, you can get your first phone number (used to send the faxes) for free. Double check that the phone number is enabled for faxes in the twilio console. Use this as your sending phone number. [[link to console]]

In this example, I'm working with phone numbers local to the united states (e.g., north america numbering plan, NANP, numbers). These phone numbers have a length of 10, such as 8002752273. If you plan to work with phone numbers in another country, you will need to change the PhoneLen value, and review Twilio's guidelines on formatting phone numbers. 

BaseUrl is the domain name and protocol Twilio will use to interact with your Django app. It needs to be accessible over the internet, so it's best to use the production protocol and hostname of your app. Heroku and DigitalOcean provide affordable hosting options for your Django/Python project. 

TwilioConfig.client is the object that we will use to communicate with Twilio. 

With the connection to Twilio setup, we are ready to look at the Fax object. 

# models
## example fax model
We are ready to start defining the fax model (e.g., a table in the django database). First, I have defined some project specific fields. In the example implementation, the fax model has a field relating a fax object to a parent project (another django model). The fax model also had a DateCreated, which defaults to the date the fax object was created. 

To send a fax, we need both a destination phone number and a Pdf of the document to send. The DestPhone field uses PhoneLen from the TwilioConfig (defined above). 

Django handles storing files, but you should define a folder to organize the Pdfs for faxes. You can change where exactly django stores files using the MEDIA_ROOT property in settings. [[link]]

Next, we need to define some fields specifically for communicating with the Twilio Fax API. 

FaxStatus holds a string indicating the current status of the fax. The default value is 'ready', but we will update it as we send the fax and interact with Twilio later. 

The TwilioID (also called SID, or FaxID) is string uniquely identifing a fax processed by twilio. The TwilioID is used to get the status of the fax, and for other API interactions. 

The TwilioError message is a long string used to hold an error message returned by Twilio. We will use this value along with the status when defining methods for the fax model. 

## model methods
It's time to get to the meat of sending a fax. More specifically, the .Send() member function on fax object. For example, myfax.send(). 

TwilioConfig.client is the Twilio Fax API client object, as defined above. We are going to create a new fax with the create() function provided by the Twilio client object. This funciton requires 3 paramaters: the sending number, the receiving number, and a full URL to a PDF to send. 

The sending number must be enabled on your twilio account, check the twilio console. In this example, the sending phone number is static and saved to the TwilioConfig object. 

The destination number is retreieved from the django fax object. For example, the fax object and the included phone number could be created using a HTML form or the Django Admin Console. 

The URL of the PDF is the combination of the base hostname for your site, and the Django provided url for the PDF file. Twilio will download the PDF from this URL. In other words, Twilio will 'callback' to this URL. 

If you have included proper paramaters, TwilioConfig.client.create() returns a twilio.fax object. We are going to save the sid of the twilio.fax object back to our django model in the TwilioId field. 

If the paramaters are invalid (e.g., invalid destination phone number), TwilioConfig.client.create() throws a TwilioId.base.exceptions.TwilioRestException exception. 

We are going to catch this exception, and update the status of the django fax object to error. Also, the error message can be saved in the TwilioErrorMessage field. Thus, if you look at a list of recently sent faxes, you can see the status of this fax is not 'delivered' but 'error'. 

Remember to save the django fax object after you are finished modifying it. 

In this example, I have returned the django fax object. You could alternatively return the status.

## update status method
Below, we will define a view that updates the status of a fax object. This view will be used by Twilio to update the status as the fax is processed (e.g., sending, delivered, no answer).

Twilio can request to update the status rapidly. For example, a fax may rapidly change from "queued" to "sending", and further to "no answer". 

To handle this, we need to lock the fax object while we are updating the status. Then, other attempts to update the status will wait for the current transaction to finish.

Note, this is supported with database backends such as MySql, and is not supported on the default Sqllite (e.g.,file) database. 

First, with transaction.atomic() creates a database transaction context. Within the transaction, we query the database for the current fax object. While it seems redundant to query for the current object (self), it's necessary to properly lock the database record for the fax object. 

After we select the fax object, update it's status value, and save it. Outside the "with" block, the fax object is unlocked.

If a second request to update the fax value comes in while the first is being processed, it will wait and automatically begin after the first is finished. 

# views

- note -- this is defined in twilio console?
- pdf view
- concept of post request?
- how to connect views to URLs in documentation?
- concept of post request, 
- how to give twilio URL, 
- how to connect URLs to views in django

## status callback revised
In the Twilio console, you can define a URL for status callbacks. When the status of an outgoing fax is updated, Twilio will callback to this URL. 

We need to create a Django view to handle this incoming reqeust from Twilio.

The purpose of this view is to find the django fax object twilio is talking about, and copy the new status provided by twilio back to the local fax object. Then, we provide an empty json response, indicating success. 

First, we extract the FaxId from the request, and find the django fax object with that has a matching TwilioId.

Then, we call the UpdateStatus() method on the found fax object, passing in the new status vaue provided by Twilio in the request. 

Now, if you have a different view that lists faxes including their status, these status values will reflect the updated values provided by Twilio. 




