{% extends "code/cpp-base.html" %}
{% block content %}
<h1 id="intro">Introduction</h1>
<p>C++ is commonly used for numerical modeling (i.e., solving complex math problems) including matrix operations. Matrix mathematics is cumbersome,
    but powerful. </p>
<p>This informal library provides matrix data structures, and defines related mathematical operations. For example,
    matrix multiplication. </p>
<p>This library utilizes C++ patterns including an abstract base class, and templating. </p>
<p>The &quot;aMatrix&quot; abstract base class allows operations to be performed on matrixes of varying types. </p>
<p>Common matrix types include dense (dMatrix), triangular (tMatrix), and diagonal (diagMatrix). A diagonal matrix has values only along the diagonal,
    and zeroes everywhere else. A triangular matrix includes values along the diagonal, and additionally non-zero values
    either above or below the diagonal. A dense matrix can have values anywhere. </p>
<a href="https://www.quora.com/What-is-meant-by-the-upper-and-lower-diagonal-elements-of-a-matrix"><img class="ui right floated bordered image" src="{{ url_for('static', filename='matrix-img.gif') }}"></a>
<p>For example, a diagonal matrix could be stored as a dense matrix. However, this is memory inefficient. A dense 4x4
    matrix is allocated space for 16 values. A diagonal 4x4 matrix is allocated only space for 4 values. </p>
<p>It&#39;s efficient to use diagonal matrixes where possible. However, it&#39;s necessary to perform operations on both
    dense and more specific matrix types. For example, the matrix multiplication function is defined to accept two
    abstract matrixes. That is, any matrix based on the aMatrix abstract base class. Thus, you can multiply a dense
    matrix by a diagonal matrix. This will call the multiplication function defined using the abstract base class, aMatrix. </p>
<p>Additionally, all of the matrix classes are templated. In other words, when creating a matrix object, you provide a
    type identifier (e.g., integer, float, double). This type identifier is used to create the objects within the
    matrix. For example, you can have a matrix of integers or float values. </p>
<p>Notably, the template parameter is separate from the matrix type. For example, you can have a diagonal matrix of
    integers, or a triangular matrix of floats. </p>
<h1 id="base-class">Abstract Base Class Definition (aMatrix)</h2>
<p>The abstract base class (aMatrix) lists member functions that all child classes have to implement. For example, all
    child classes of aMatrix have to implement matrix.get(i,j), where i and j are indexes of a location in the matrix.
</p>
<div class="ui top attached code orange segment">
    <p>C++, aMatrix.hpp</p>
</div>
<div class="ui bottom attached code segment">
    {{ cpp_block('code/cpp-samples/abstract-def.hpp') }}
</div>
<h3 id="member">Member Functions</h3>

<p>Thus, we can define matrix multiplication between two aMatrixes, knowing that regardless of the actual type of matrix
    eventually used, they will implement get(i,j). This function returns a copy of the value at that index in the matrix. </p>
<p>Additionally, we will define a function element(i,j) that returns a reference, allowing you to modify values in the
    matrix. </p>
<p>The size() and valid(i,j) functions make accessing the matrix easier. </p>
<h3 id="friend">Friend Functions for Abstract Matrixes</h3>
<p>Related to the abstract base class, we are defining functions that accept take aMatrix as input parameters. In other
    words, they accept any matrix type based on aMatrix as an input.</p>
<div class="ui top attached code orange segment">
    <p>C++, aMatrix.hpp</p>
</div>
<div class="ui bottom attached code segment">
    {{ cpp_block('code/cpp-samples/abstract-friend.hpp') }}
</div>

<p>For example, the operator* function allows you to multiply two matrixes of any type. A dense matrix (dMatrix) is
    returned. </p>
<p>Because we don&#39;t know the type of the matrixes input, we can only use functions declared at the aMatrix level.
    For example, since aMatrix requires size(), get(), and element(), we can use those functions. The child classes of
    aMatrix, like diagMatrix, are free to implement these functions in their own way. </p>
<h1 id="implementation-classes">Implementation Classes</h1>
<h2 id="diag">Diagonal Matrix (diagMatrix)</h2>
<p>Now that the abstract base class is defined, consider an implementation. The class 'dMatrix' inherits from the aMatrix base class, and is defined below.</p>
<div class="ui top attached code orange segment">
    <p>C++, dMatrix.hpp file</p>
</div>
<div class="ui bottom attached code segment">
    {{ cpp_block('code/cpp-samples/diag-def.hpp') }}
</div>
<p>Let&#39;s take a look at an example implementation of class that inherits from aMatrix.</p>
<p>The diagMatrix class describes a diagonal matrix, and is generally a sub-type of matrixes. </p>
<p>The diagMatrix only stores the values along the diagonal, but because it is based on aMatrix, you can use it
    alongside other types of matrixes. For example, you can multiply a dense matrix by a diagMatrix. </p>
<p>The aMatrix base class is abstract, so it doesn&#39;t have any member variables. In contrast, the diagMatrix class inherits from aMatrix, but isn&#39;t abstract, it actually implements a matrix. So, it has
    member variables, and a constructor. </p>
<p>The constructor, diagMatrix<T>::diagMatrix(i), allocates just enough memory to store the diagonal, equal to the size
        of the matrix. Dense matrixes have data allocated for each location in the matrix, equal to the size squared.
</p>
<h3 id="helper">Helper Functions for diagMatrix</h3>
<div class="ui top attached code orange segment">
    <p>C++, dMatrix.hpp file</p>
</div>
<div class="ui bottom attached code segment">
    {{ cpp_block('code/cpp-samples/diag-help.hpp') }}
</div>
<p>Before we discuss the access/modify functions (i.e., get and element), some helper functions are necessary. They make
    the accessor functions easier to read. </p>
<p>Valid() indicates if a provided index is within the matrix. For example, (-1, 38) is not a valid matrix index. </p>
<p>Defined() indicates if the index points to a location that has memory allocated. For example, in a diagonal matrix,
    every location besides the diagonal is zero, and thus doesn&#39;t have memory allocated. </p>
<p>The map function transforms a matrix index, such as (i,j), to an array index. In this implementation, the underlying
    data structure is an array. More specifically, the values of the diagonal are stored as an array. The map function
    takes a matrix index and maps it to a location in the array. </p>
<h3 id="access">Access Functions</h3>
<div class="ui top attached code orange segment">
    <p>C++, dMatrix.hpp file</p>
</div>
<div class="ui bottom attached code segment">
    {{ cpp_block('code/cpp-samples/diag-get.hpp') }}
</div>
<p>Now, consider the access function get(i, j). It&#39;s quite readable with some helper functions defined.</p>
<p>First, if the index is invalid, we throw an error.</p>
<p>If the index is valid, and also defined, we use the map function to retrieve the value from our array data structure.
</p>
<p>If the index is valid, but not defined, we simply return zero. For example, any index not along the diagonal is
    zero, in this type of matrix.</p>
<p>Note this get() function returns a copy of the value from the matrix. </p>
<p>In contrast, element() returns a mutable (i.e., editable) reference to the location in the matrix.</p>
<p>If we want to retrieve a modifiable reference, the logic is slightly different. We can&#39;t return a modifiable reference to a location that must be zero. Thus, we throw an error if the location
    is invalid, or not defined. </p>
<h1 id="special-cases">Overloading for Special Cases</h1>

<p>The general matrix multiplication function, for two aMatrixes, is described above.</p>
<p>However, a useful feature of C++ is that you can &#39;overload&#39; functions. In other words, you can create a
    separate implementation for a particular situation. </p>
<p>Generally, when you multiply a diagonal matrix by a triangular matrix, you get another triangular matrix as a
    result. With this knowledge, we can define another matrix multiplication function, that will return a more efficient
    triangular matrix as the result.</p>
<p>C++ will automatically use this more efficient function where possible, because the syntax is the same.</p>
<p>result = densematrix * mytriangle //uses the general implementation from the aMatrix level</p>
<p>result = mydiag * mytriangle //uses the &#39;overloaded&#39; implementation we additionally defined</p>
<p>In the below example, we use result.defined() to skip calculating values that will be zero. </p>
<div class="ui top attached code orange segment">
    <p>C++, mult.hpp file</p>
</div>
<div class="ui bottom attached code segment">
    {{ cpp_block('code/cpp-samples/special-mult.hpp') }}
</div>
<h1 id="testing">Testing</h1>
<p>A matrix library is only useful if generates accurate results. </p>
<p>Creating unit tests is an efficient way to check that your library is working properly. </p>
<p>This example uses Boost Unit Tests, and test matrix multiplication for dense matrixes. Let's walk through creating a unit test for matrix multiplication.</p>
<div class="ui top attached code orange segment">
    <p>C++, test.hpp file</p>
</div>
<div class="ui bottom attached code segment">
    {{ cpp_block('code/cpp-samples/test-diag.hpp') }}
</div>
<p>First, define the test data. The variables d1 and d2 are the test data used to create the two matrices that will be multiplied
    together. The variables d3 and d4 store the correct results, used to check the result of our multiplication. Create the two matrixes tc1 and tc2 from the test data. </p>
<p>Then, create the multiplication results, using the * operator defined above for aMatrixes. The dense matrices (dMatrix) will match the aMatrix definition, due to the class inheritance. </p>
<p>Finally, compare the result of the multiplication to the known correct data stored in d3 and d4. </p>
<p>Use the BOOST_CHECK_EQUAL(a, b) function to check if two values are equal. Iterate through the matrix to check all the values.</p>
<p>When you run the test suite, boost handles outputting the results. For example, it could indicate all test cases
    passed, or the &#39;dm_mult&#39; test case failed. It is a best practice to run the tests before committing, to
    make sure you haven&#39;t accidentally broken some functionality. </p>
<h1>Conclusion</h1>
<p>Additional implementations of the abstract matrix base class are provided in the git repository. Matrix types such as symmetric and dense are defined. This library is ready to be used to store matrixes for numerical modeling programs. The next step for this project is to implement a matrix solver function.</p>




{% endblock %}