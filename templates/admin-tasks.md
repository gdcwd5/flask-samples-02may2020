 # setup provider

Administration > "provider / Trunks" > "add provider"

- input the provider name, and mark the provider as active
- select the gateway type of "SIP" for faxing

- use the username and password supplied by your fax SIP provider
- use the fax hostname and port supplied by the fax SIP provider


- set the new SIP provider as the default
- leave the prefix field blank
- select the weight of 0

- enable register, to allow incoming faxes

# add users
Administration > "user management" > "add user"

- select a username, for logging into web portal
- select an initial password

- enter user profile information, including email
- the email address is used for notifications of received faxes, and to send outgoing faxes by email

- choose the appropriate country and time zone

- mark the user active

- select the "user" or "admin" role as appropriate. Admins can manage providers, users, and phone numbers.

# add phone numbers
"DID Numbers" > "Add DID"

- enter the phone number, in the format used by the SIP provider. The standard is country code, followed by the country formatted number. In the US, this is a 3 digit area code followed by a 7 digit local number. For example, "18002752273". 

- mark the number as active

- important!
- find number in list, click icon, assign to existing user

- this allows them to send numbers from the fax, and enables email alerts for incoming faxes

