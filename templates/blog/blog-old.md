<h3 id="remote-extensions-">Install Extensions on the VM</h3>
                <p>Earlier, we installed the &quot;Remote - SSH&quot; extension locally.</p>
                <p>Now,  we can install extensions on the remote machine. Remote extensions can see the libraries and files on the remote VM.</p>
                <p>VS code is a lightweight editor like Sublime or Atom, but you can easily add the full power of
                    Microsoft&#39;s
                    IntelliSense IDE tools.</p>
                <p>I&#39;m using python, so I&#39;ll add the Microsoft Python extension to the remote machine. Other
                    extensios
                    include JavaScript, TypeScript, C/C++, Go, and more! Also install pylint in the project (i.e., via
                    pip)</p>
                <p>Once it reloads, you can see syntax errors, suggestions, and more while editing. </p>
                <a href="https://gyazo.com/7bdc36f23d1d3bd6dffab3ce1334dc79"><video alt="Video from Gyazo" width="422"
                        autoplay muted loop playsinline controls>
                        <source src="https://i.gyazo.com/7bdc36f23d1d3bd6dffab3ce1334dc79.mp4" type="video/mp4" />
                        </video></a>

l8r:
- port issue? dropdown?
- create hostname?
- accessability

- add download to intro
- talk about extensions in VS Code in conclusion