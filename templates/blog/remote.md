# outline

- setup digital ocean
    - domain name?
- passwordless
    - ssh on windows
- open in vscode, connect
    - auto setup
    - now you can (terminal, browse)
- choose a workspace folder
    - or clone in git
- how extensions work
    - remote extensions?
- run it
    - run.json or terminal?
- view it!
    - forward ports for webserver
    - debug

- next steps
    - debugging
    - collab! 

# write
- compelling intro?

# screenshots
- setup
    - create ssh key x
    - copy key x 
    - paste key in DO x
    - create VM in DO [nah]
- install extension x
- connect button x 
- ssh command x
- installing progress x
- extension? x
- run and click link x
- view webpage x

## setup VM
I host my linux development virtual machine (VM) in Digital Ocean. It's affordable and easy to use. However, your dev machine can be any machine you can open a SSH connection to. 

Create a ubuntu VM in digital ocean. [[reorder?]] They allow you to specify SSH keys during creation, so you can be secure and #passwordless. 

Creating your ssh key is easier than ever in Windows 10. Open Windows Terminal, run "ssh-keygen” and save the files to the default location. 

Open the id_rsa.pub file in a text editor. It's stored in your user directory, under .ssh. Copy the whole thing. 

Open "digital ocean > settings > security > add ssh key" and paste the copied key. Give it a descriptive name for the computer you are using. [[!! new ssh key option in create droplet]]

Then, create the VM (navigate to doplets) and create a ubuntu VM. Select the SSH key. 

- Cover domain name? breakout? or ip address?

## vs code
- download and install VS code if you haven't already

- open extensions, and install "Remote - SSH"
    - reload?

Click on the remote connect icon in the bottom left, this is where you will see the hostname of the machine you are connected to. 

Select "remote-ssh connect to host" and then choose new connection. 

Here, enter the command you would usually enter in a terminal, eg, linux terminal.

"ssh [user]@[domain or password] --options"

- mention port thing? firewall?

- gif of connecting

VS Code will automatically install everything needed on the machine you are connecting to. 

Once you are connected, open the folder view.

You can access existing directories, or clone a git repo. 

Let's open an existing git repo. Make sure to open the base folder of the project, so VS code can automatically find files. 

## remote extensions?
Earlier, we installed the "Remote - SSH" extension locally.

Now that we can install extensions on the remote machine. Then, they can see the libries and files on the remote machiene. 

VS code is leightweight like sublime or atom, but you can easily add the full power of Microsoft's IntelliSense IDE tools.

I'm using python, so I'll add the Microsoft Python extension to the remote machine. Other extensios include JavaScript, TypeScript, C/C++, Go, and more! Also install pylint in the project (i.e., via pip)

Once it reloads, you can see syntax errors, suggestions, and more while editing. 

## run and view
You can use the built in terminal (ctrl `) to build and run your applications.

However, you may be wondering how you access web applications running on the remote machine.

VS Code automatically sets up SSH tunnels, so you can access applications running on ports like localhost:5000 or 127.0.0.1:8000. Click on the link in the terminal, and the connection opens. Just like you were running it locally. 

Notice the URL is to local machine, but VS Code forwards it to the remote machine.

disconnect, close window or use button. You can have multiple windows, local, remote, different remote, combination etc. 

# nty 
- run json
- explain exteions more
- setup domain name

# tasks
- screenshots? gifs?
- describe?
- how to keep under 500 words?
- accessability? screenshot described?
